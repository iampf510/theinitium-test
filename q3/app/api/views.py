from django.shortcuts import render
from api.models import Timeapi
from rest_framework import viewsets
from rest_framework.views import APIView
from django.http import HttpResponse
from django.db import connection, transaction
import json

class TimeapiViewSet(viewsets.ModelViewSet):

    def list(self, request, *args, **kwargs):

        cursor = connection.cursor()

        cursor.execute('SELECT NOW(), @@session.time_zone;')
        t = cursor.fetchone()

        return HttpResponse(json.dumps({'time':t[0].isoformat(), 'timezone':t[1] }))



