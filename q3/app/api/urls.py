from django.urls import include,path
from rest_framework.routers import DefaultRouter
from api.views import TimeapiViewSet

router = DefaultRouter()
router.register('time', TimeapiViewSet, base_name='timeapi')

urlpatterns = [
            path('',include(router.urls)),
            ]
