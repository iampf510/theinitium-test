
def transDic(string):
    dic = {}
    for items in string.split('|'):
        key, value = items.split(':')
        value = int(value)
        dic[key] = value
    return dic


if __name__ == '__main__':
    string = 'k:1|k1:2|k2:3|k3:4'

    dic = transDic(string)

    print(dic)




