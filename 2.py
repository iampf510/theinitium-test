
def changeStyle(string):
    return ''.join([ s.title() for s in string.split('_')])

if __name__ == '__main__':
    string = 'open_door'

    string = changeStyle(string)
    print(string)
